// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Project_TDGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_TD_API AProject_TDGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
